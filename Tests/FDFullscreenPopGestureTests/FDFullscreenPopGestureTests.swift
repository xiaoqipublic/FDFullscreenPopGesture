import XCTest
@testable import FDFullscreenPopGesture

final class FDFullscreenPopGestureTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(FDFullscreenPopGesture().text, "Hello, World!")
    }
}
